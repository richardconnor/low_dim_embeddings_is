function randoms = getRandomElements(data,number,seed)
% return number of the elements of data, where each row of data is an
% element; use seed to reseed random generator if supplied


[m,w] = size( data );
indices = randi(m,1,number);

randoms = zeros(number,w);
for i = 1 : number
    randoms(i,:) = data(indices(i),:);
end

end

