function createPlot( dataToPlot,pDist,highlightPoints,colours)

%%

testData=[1,2,1;2,1,1];

    function passed(data)
        passed = true;
    end


%% draw the scatter diagrams for figure 5 in the IS paper


%plot the pivot points
scatter([0,pDist],[0,0],50,'black','filled');


% plot all the rest of the points
scatter(dataToPlot(:,1),dataToPlot(:,2),50,[0,0,0]);


% plot the highlighted points
for i = 1 : 5
    scatter(dataToPlot(highlightPoints(i),1),dataToPlot(highlightPoints(i),2),200,colours(i),'filled');
end


end

