function [count,distances] = countWithinRadius(twoDdata,centre,threshold)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

[dataSize,two] = size(twoDdata);
distances = zeros(dataSize,1);
count = 0;

for i = 1 : dataSize
    d = euc(twoDdata(i,:),centre);
    distances(i) = d;
    if d < threshold
        count = count + 1;
    end
end

end

