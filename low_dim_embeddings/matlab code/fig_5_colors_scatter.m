
%% set the generic data parameters
dataSeries = {'SISAP colors', 'Euc10', 'Euc20','mf\_fc6'};
choice = 1;

dataSize = 500;
noOfPivots = 200;

switch choice
    case 1
        thisData = getSisapData('colors');
    case 2
        thisData = getSisapData('euc',10,100000);
    case 3
        thisData = getSisapData('euc',20,100000);
    otherwise
        thisData = mf_fc6_raw(:,2:4097);
end

ref_points = getRandomElements(thisData,noOfPivots);
data_points = getRandomElements(thisData,dataSize);

%%

metric = @emd;

%% set threshold info

switch choice
    case 1
        thresholds = getThresholds('colors');
        threshold = thresholds(1);
    case 2
% Euc 10 threshold one per million
        threshold = 0.24;
    case 3
% Euc 20 threshold one per million
        threshold = 0.6;
    otherwise
        threshold = 100;
end


%% here we set a few distinguished objects and highlighted colours to plot them...

highlightPoints = [100,200,300,400,500];
colours = ['r','g','b','m','c'];

highlitPoint = 5;

%% if we want specific pivots...

pivot1 = 55;
pivot2 = 66;


pivot1 = 24;
pivot2 = 127;


%% find best and worst pivot pairs for a given data point

countMin = 100000;
countMax = 0;
bestPivs = [0,0];
worstPivs = [0,0];
for pivot1 = 1 : 149
    for pivot2 = pivot1 + 1 : 150
        [dataToPlot, pDist] = form2Dprojection(metric, pivot1,pivot2,ref_points, data_points, dataSize);

        circleCentre = dataToPlot(highlightPoints(highlitPoint),:);
        [count,distsFromCentrePoint] = countWithinRadius(dataToPlot,circleCentre,threshold);
        if count < countMin
            countMin = count;
            bestPivs(1) = pivot1;
            bestPivs(2) = pivot2;
        end
        if count > countMax
            countMax = count;
            worstPivs(1) = pivot1;
            worstPivs(2) = pivot2;
        end
    end
end

pivot1 = bestPivs(1);
pivot2 = bestPivs(2);

%% do the heavy lifting

[dataToPlot, pDist] = form2Dprojection(metric, pivot1, pivot2, ref_points, data_points, dataSize);

%% calculate a centre for a circle to be plotted, also how many points are within a threshold radius

circleCentre = dataToPlot(highlightPoints(highlitPoint),:);

[count,distsFromCentrePoint] = countWithinRadius(dataToPlot,circleCentre,threshold);


%% draw the scatter diagrams for figure 5 in the IS paper

fig = figure;
set(gca,'FontSize',14);
hold on;
axis equal;

createPlot(dataToPlot, pDist, highlightPoints, colours);

plotCircle(circleCentre,threshold,'r',2);
% 
% %now plot circle
% r = threshold;
% xc = circleCentre(1,1);
% yc = circleCentre(1,2);
% 
% theta = linspace(0,2*pi);
% x = r*cos(theta) + xc;
% y = r*sin(theta) + yc;
% plot(x,y,'r','LineWidth',5);


hold off;

title(dataSeries(choice),'FontSize',20)
xlabel('X', 'FontSize',16);
ylabel('altitude from line (p_1,p_2)', 'FontSize',16)
%%  save directly into the file system, uncomment with caution!

% file_name = strcat('colors_left_exc','.png');
% dir_path = '/Users/newrichard/Dropbox/superExtremePapers/concepts/figs/';
% dir_path2 = '/Users/newrichard/Dropbox/nPointJournalPaper/figs/';

% saveas(h,strcat(dir_path,file_name));


%%
%so let's see if we can swing this into a better orientation...

coeff1 = pca(dataToPlot);
coeff2 = polyfit(dataToPlot(:,1),dataToPlot(:,2),1);

xmeans = mean(dataToPlot(:,1));
ymeans = mean(dataToPlot(:,2));
xdiffs = xmeans - dataToPlot(:,1);
ydiffs = ymeans - dataToPlot(:,2);


%% just have a wee look at the X and Y spreads...
% 
% figure
% histogram(dataToPlot(:,1));
% figure
% histogram(dataToPlot(:,2));

