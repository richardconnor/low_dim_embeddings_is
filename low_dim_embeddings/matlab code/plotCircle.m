function [outputArg1,outputArg2] = plotCircle(centre,radius, colour, lineWidth)


%plot circle
xc = centre(1,1);
yc = centre(1,2);

theta = linspace(0,2*pi);
x = radius * cos(theta) + xc;
y = radius * sin(theta) + yc;

plot(x,y,colour,'LineWidth',lineWidth);

end

