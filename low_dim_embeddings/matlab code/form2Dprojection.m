function [dataToPlot, pDist] = form2Dprojection(metric, pivot1, pivot2,ref_points, data_points, dataSize)
%return the 2D projection based on input data and pivots
%   Detailed explanation goes here

pDist = metric(ref_points(pivot1,:),ref_points(pivot2,:));

dists = zeros(dataSize,2);
for i = 1 : dataSize
    dists(i,1) = metric(ref_points(pivot1,:),data_points(i,:));
    dists(i,2) = metric(ref_points(pivot2,:),data_points(i,:));
end


% now convert all data points into XY coordinates based on pivot distances
% also tracking the index and X coordinate of the leftmost point


dataToPlot = zeros(dataSize,2);
for i = 1 : dataSize
    d1 = dists(i,1);
    d2 = dists(i,2);
    
    %a^2 = b^2 + c^2 - 2bc cosA
    % so.... cosA = (b^2 + c^2 - a^2)/2b
    x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
    y_offset = sqrt(d1*d1 - x_offset * x_offset);
    
    dataToPlot(i,1) = x_offset;
    dataToPlot(i,2) = y_offset;
    
end

end

