package extreme_fourpoint;

import java.util.ArrayList;
import java.util.List;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.simplices.NSimplex;


public class EdgarIdeaFirstClosestPivotsChooser<T> extends RefPointsSelector<T> {
	

	
	/**
	 * @param refPoints
	 * @param metric
	 */
	EdgarIdeaFirstClosestPivotsChooser(List<T> refPoints, Metric<T> metric) {
		super(refPoints, metric);
	}

	/* (non-Javadoc)
	 * @see extreme_fourpoint.RefPointsSelector#setWitnesses(java.util.List)
	 */
	@Override
	public void setWitnesses(List<T> witnesses) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see extreme_fourpoint.RefPointsSelector#getDataPoint(java.lang.Object)
	 */
	@Override
	public DataPoint<T> getDataPoint(T datum) {
		DataPoint<T> result = new DataPoint<>();
		result.datum = datum;

		//select as first pivot the closest one
		int idFirstPivot=0;
		double minDist=Double.MAX_VALUE;
		double[] refDists = new double[this.refPoints.size()];
		for (int i = 0; i < this.refPoints.size(); i++) {
			refDists[i] = this.metric.distance(datum, this.refPoints.get(i));
			if(refDists[i]<minDist) {
				minDist=refDists[i];
				idFirstPivot=i;
			}
		}
		
		//Let p1 the first pivot. select as second pivot the refernce object  p that maximize |d(p1,u) - d(p,p1) | 
		int idSecondPivot=0;
		double maxCondValue=0;
		for (int i = 0; i < this.refPoints.size(); i++) {
			 double diff= Math.abs(minDist-refDists[i]);
			 if(diff>maxCondValue) {
				 idSecondPivot=i;
				 maxCondValue=diff;
			 }
		}
			
		//result.pivotId = p.pivotId;

		int[] sd = { idFirstPivot, idSecondPivot };
		result.refs = sd;
		
		List<T> pivs = new ArrayList<>();
		pivs.add(refPoints.get(idFirstPivot));
		pivs.add(refPoints.get(idSecondPivot));
		
		double[] dds = { refDists[idFirstPivot], refDists[idSecondPivot] };
		result.dists = dds;
		
		NSimplex simp=new NSimplex<>(this.metric, pivs);
		result.apex = simp.getApex(dds);

		return result;	
	}
	
	public double[] getRefDists(T datum) {
		double[] pointToPivDists = new double[this.refPoints.size()];
		for (int i = 0; i < this.refPoints.size(); i++) {
			pointToPivDists[i] = this.metric.distance(datum, this.refPoints.get(i));
		}
		return pointToPivDists;
	}

	
//	public void writePivotDistanceTable(String filename) throws IOException {
//		float[][] dists = new float[this.refPoints.size()][this.refPoints
//				.size()];
//		for (int i = 0; i < this.refPoints.size() - 1; i++) {
//			for (int j = i + 1; j < this.refPoints.size(); j++) {
//				double d = this.metric.distance(this.refPoints.get(i),
//						this.refPoints.get(j));
//				dists[i][j] = (float) d;
//			}
//		}
//		OutputStream fw = new FileOutputStream(filename);
//		ObjectOutputStream oos = new ObjectOutputStream(fw);
//		oos.writeObject(dists);
//		oos.close();
//	}
	

	
}
