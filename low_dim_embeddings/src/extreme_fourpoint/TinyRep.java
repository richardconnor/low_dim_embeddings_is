package extreme_fourpoint;

public class TinyRep {

	private int maxRef;

	private static int signMask = 0x80000000;
	private static int exponentMask = 0x7f800000;
	private static int mantissaMask = 0x007fffff;

	// taking top four, and bottom 5, bits of the float
	private static int cheekyBits = 0xf0000007;
	private static int top2bits = 0xc0000000;
	private static int top4bits = 0xf0000000;
	private static int top2of7 = 0x60;
	private static int bottom5 = 0x1f;
	private static int top4of7 = 0x78;
	private static int bottom3 = 0x07;

	private static int exponentShift = 23;
	private static int topOfRefShift = 25;

	TinyRep(int maxRef) {
		try {
			assert false;
			throw new RuntimeException("assertions not enabled");
		} catch (AssertionError e) {
			// oh good, assertions are enabled!
		}
		this.maxRef = maxRef;
	}

	public static int getRep(int reference, double distance) {
		// assert distance < 2 && distance >= 4.7e-10;
		float dist = (float) distance;

		int fBits = Float.floatToIntBits(dist);

		/*
		 * the exponent
		 */
		int exponent = (fBits & exponentMask) >> exponentShift;
		// assert exponent >= 96;
		// expRep is in 0..31, ie bottom 5 bits
		int expRep = Math.max(0, exponent - 96);
		assert expRep <= 31;
		fBits = fBits & (~exponentMask ^ (expRep << exponentShift));

		// check - these should now all be zeros
		assert fBits == (fBits & ~top4bits);

		int top = (reference & top4of7) << topOfRefShift;
		int bottom = reference & bottom3;

		fBits = fBits ^ top;
		fBits = (fBits & ~bottom3) ^ bottom;

		return fBits;
	}

	public static int getRef(int rep) {

		int res = (rep & top4bits) >>> topOfRefShift;
		int res1 = rep & bottom3;

		return res ^ res1;
	}

	public static float getDist(int rep) {
		// remove id info and set all bottom bits to maximise distance within
		// possible original range
		int res = (rep & ~cheekyBits) ^ bottom3;

		// recalculate original exponent
		int expRep = ((res & exponentMask) >> exponentShift) + 96;
		// System.out.println("er in getDist " + expRep);

		// and replace it into float exponent field
		res = (res & ~exponentMask) ^ (expRep << exponentShift);
		return Float.intBitsToFloat(res);
	}

	public static void main(String[] a) {

		TinyRep tr = new TinyRep(127);
		int rep = tr.getRep(111, 1e-15);
		// System.out.println(rep);
		int ref = tr.getRef(rep);
		System.out.println(ref);
		System.out.println(tr.getDist(rep));

	}
}
