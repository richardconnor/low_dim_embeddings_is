package extreme_fourpoint;

public enum DataSets {
	colors, nasa, euc10, euc20, sift, gist, yfcc, alexnet
}
