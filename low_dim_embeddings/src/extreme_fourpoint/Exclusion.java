package extreme_fourpoint;

import java.io.Serializable;

/**
 * @author newrichard
 * 
 *         DO NOT CHANGE THIS CLASS AS IT HAS BEEN USED TO STORE BIG DATA!!!
 */
public class Exclusion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private short ref1, ref2;
	private float x, y;

	Exclusion(short ref1, short ref2, float x, float y) {
		this.ref1 = ref1;
		this.ref2 = ref2;
		this.x = x;
		this.y = y;
	}

	public int getRef1() {
		return this.ref1;
	}

	public int getRef2() {
		return this.ref2;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

}
