package extreme_fourpoint;

import java.util.ArrayList;
import java.util.List;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.simplices.NSimplex;


public class EdgarIdeaFirstClosestSecondFurthestPivotsChooser<T> extends RefPointsSelector<T> {
	

	
	/**
	 * @param refPoints
	 * @param metric
	 */
	EdgarIdeaFirstClosestSecondFurthestPivotsChooser(List<T> refPoints, Metric<T> metric) {
		super(refPoints, metric);
	}

	/* (non-Javadoc)
	 * @see extreme_fourpoint.RefPointsSelector#setWitnesses(java.util.List)
	 */
	@Override
	public void setWitnesses(List<T> witnesses) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see extreme_fourpoint.RefPointsSelector#getDataPoint(java.lang.Object)
	 */
	@Override
	public DataPoint<T> getDataPoint(T datum) {
		DataPoint<T> result = new DataPoint<>();
		result.datum = datum;

		//select as first pivot the closest one
		//select as second pivot the furthest one
		int idClosestPivot=0;
		int idFurthestPivot=0;
		
		double minDist=Double.MAX_VALUE;
		double maxDist=Double.MIN_VALUE;
		double[] refDists = new double[this.refPoints.size()];
		for (int i = 0; i < this.refPoints.size(); i++) {
			refDists[i] = this.metric.distance(datum, this.refPoints.get(i));
			if(refDists[i]<minDist) {
				minDist=refDists[i];
				idClosestPivot=i;
			}
			if(refDists[i]>maxDist) {
				maxDist=refDists[i];
				idFurthestPivot=i;
			}
		}
		

		int[] sd = { idClosestPivot, idFurthestPivot };
		result.refs = sd;
		
		List<T> pivs = new ArrayList<>();
		pivs.add(refPoints.get(idClosestPivot));
		pivs.add(refPoints.get(idFurthestPivot));
		
		double[] dds = { refDists[idClosestPivot], refDists[idFurthestPivot] };
		result.dists = dds;
		
		NSimplex simp=new NSimplex<>(this.metric, pivs);
		result.apex = simp.getApex(dds);

		return result;	
	}
	
	public double[] getRefDists(T datum) {
		double[] pointToPivDists = new double[this.refPoints.size()];
		for (int i = 0; i < this.refPoints.size(); i++) {
			pointToPivDists[i] = this.metric.distance(datum, this.refPoints.get(i));
		}
		return pointToPivDists;
	}

	
//	public void writePivotDistanceTable(String filename) throws IOException {
//		float[][] dists = new float[this.refPoints.size()][this.refPoints
//				.size()];
//		for (int i = 0; i < this.refPoints.size() - 1; i++) {
//			for (int j = i + 1; j < this.refPoints.size(); j++) {
//				double d = this.metric.distance(this.refPoints.get(i),
//						this.refPoints.get(j));
//				dists[i][j] = (float) d;
//			}
//		}
//		OutputStream fw = new FileOutputStream(filename);
//		ObjectOutputStream oos = new ObjectOutputStream(fw);
//		oos.writeObject(dists);
//		oos.close();
//	}
	

	
}
