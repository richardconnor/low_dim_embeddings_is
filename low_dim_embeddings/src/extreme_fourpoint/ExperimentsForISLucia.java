package extreme_fourpoint;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.similarity.msc.data.cartesian.CartesianPoint;
import extreme_fourpoint.RefPointsSelector.DataPoint;

public class ExperimentsForISLucia {
	public static Log log = LogFactory.getLog(ExperimentsForISLucia.class);

	//RICHARD PATHS
//	public static String rootDir = "/Volumes/Data/superExtreme/testData/";
//	public static String SISAP_FILES_FOLDER = "/Volumes/Data/SISAP databases/dbs";
//	public static String COLORS_FILE = SISAP_FILES_FOLDER + "/vectors/colors/colors.ascii";
//	public static String NASA_FILE = SISAP_FILES_FOLDER + "/vectors/nasa/nasa.ascii";
//	public static String YFCC_DATA_FILE = "/docsNotOnIcloud/Research/yfcc/data1_20k.obj";
	
	//LUCIA PATHS
	public static String rootDir = "D:/Work/Dev/EXPERIMENTS/Datasets/superExtreme/testData/";
	public static String SISAP_FILES_FOLDER = "D:/Work/Dev/EXPERIMENTS/Datasets/superExtreme/testData/SISAP databases/dbs";
	public static String COLORS_FILE = SISAP_FILES_FOLDER + "/vectors/colors/colors.ascii";
	public static String NASA_FILE = SISAP_FILES_FOLDER + "/vectors/nasa/nasa.ascii";
	public static String YFCC_DATA_FILE =  "/docsNotOnIcloud/Research/yfcc/data1_20k.obj";

	public static void main(String[] args) throws FileNotFoundException, Exception {
		log.info("started");

//		initialiseTestData();
	//	createNNFiles();		
//	
		String[] pivSelectionMethods= {
//				"random",
//				"largestDistanceToTheThirdNearest2DPoint",
				"EdgarIdeaFirstClosest",
				"EdgarIdeaFirstFurthest",
				"EdgarIdeaFirstClosestSecondFurthest"
		};
		
		System.out.println("PivSelectionMethod\t ExclusionRate  \t FN_Prop_DistExaustiveSearch \t TruePositive/GTRes \t ElapsedTime(ms) \t #non-exclusions per query");
		for(String pivSelectionMethod: pivSelectionMethods) {
			ExperimentalContext_IS_paperLucia ec = new ExperimentalContext_IS_paperLucia(DataSets.colors, 30); //euc20
			generateSurrogateData(ec,pivSelectionMethod );
			ec.generatePivotTableData(pivSelectionMethod);
			doSurrogateQueries(ec, 1, true,pivSelectionMethod);
		}
		//log.info("ended");
	}

	

	@SuppressWarnings("unused")
	private static void createNNFiles() throws Exception {
		ExperimentalContext_IS_paperLucia.createNNinfo("nasa", 10);
		ExperimentalContext_IS_paperLucia.createNNinfo("colors", 10);
//		ExperimentalContext_IS_paperLucia.createNNinfo("euc10", 10);
//		ExperimentalContext_IS_paperLucia.createNNinfo("euc20", 10);
		ExperimentalContext_IS_paperLucia.createNNinfo("sift", 10);
	}

	@SuppressWarnings("unused")
	private static void initialiseTestData() throws FileNotFoundException, Exception {
		ExperimentalContext_IS_paperLucia.setUpData(DataSets.colors);
		ExperimentalContext_IS_paperLucia.setUpData(DataSets.nasa);
//		ExperimentalContext_IS_paperLucia.setUpData(DataSets.euc10);
//		ExperimentalContext_IS_paperLucia.setUpData(DataSets.euc20);
		ExperimentalContext_IS_paperLucia.setUpData(DataSets.sift);

	}

	/**
	 * generates the XY coordinate data for the given data set, based on
	 * it used a pivot selection method specified in pivSelectionMethod
	 * 
	 * @param ec     this is used to get pivots, witnesses, data
	 * @param serial
	 * @throws Exception
	 */
	private static void generateSurrogateData(ExperimentalContext_IS_paperLucia ec,
			String pivSelectionMethod) throws Exception {
		List<CartesianPoint> wits = ec.getWitnesses();
		List<CartesianPoint> dat = ec.getBigData();
		// moderated by ec, not all available pivots!
		List<CartesianPoint> pivs = ec.getPivots();

		
		RefPointsSelector<CartesianPoint> pivotsChooser=null;;
		switch (pivSelectionMethod) {
			case "largestDistanceToTheThirdNearest2DPoint" :
				pivotsChooser= new FourPointPivotsChooser<>(pivs, ec.metric, 0, false, 3);
				break;
			case "random":
				pivotsChooser= new RandomRefPointsChooser<>(pivs, ec.metric);
				break;
			case "EdgarIdeaFirstClosest":
				pivotsChooser= new EdgarIdeaFirstClosestPivotsChooser<>(pivs, ec.metric);
				break;
			case "EdgarIdeaFirstFurthest":
				pivotsChooser= new EdgarIdeaFirstFurthestPivotsChooser<>(pivs, ec.metric);
				break;
			case "EdgarIdeaFirstClosestSecondFurthest":
				pivotsChooser=new EdgarIdeaFirstClosestSecondFurthestPivotsChooser<>(pivs, ec.metric);
				break;
			default :
				throw new Exception("Not implemented yet!");
		}
		
		pivotsChooser.setWitnesses(wits);

		StoredExclusions se = new StoredExclusions();

		int size = dat.size();
		se.dist1s = new float[size];
		se.dist2s = new float[size];
		se.piv1s = new short[size];
		se.piv2s = new short[size];
		for (int i = 0; i < dat.size(); i++) {
			if (i % 100 == 0) {
				// System.out.println("doing " + i);
			}
			CartesianPoint d = dat.get(i);
			DataPoint<CartesianPoint> x = pivotsChooser.getDataPoint(d);
			se.dist1s[i] = (float) x.dists[0];
			se.dist2s[i] = (float) x.dists[1];
			se.piv1s[i] = (short) x.refs[0];
			se.piv2s[i] = (short) x.refs[1];
		}
		OutputStream fw = new FileOutputStream(ec.getSurDataFileName(pivSelectionMethod));
		ObjectOutputStream oos = new ObjectOutputStream(fw);
		oos.writeObject(se);
		oos.close();
		
	}

	public static void doSurrogateQueries(ExperimentalContext_IS_paperLucia ec, int totalTrue, boolean checkDists, String pivSelectionMethod)
			throws Exception {

		String surDataTable = ec.getSurDataFileName(pivSelectionMethod);
		FileInputStream fis2 = new FileInputStream(surDataTable);
		ObjectInputStream ois2 = new ObjectInputStream(fis2);

		float[][] pivDists = ec.getPivotDistances(pivSelectionMethod);
		StoredExclusions se = (StoredExclusions) ois2.readObject();
		fis2.close();
		int dataSize = se.dist1s.length;

		List<CartesianPoint> queries = ec.getQueries();
		List<CartesianPoint> pivs = ec.getPivots();
		List<CartesianPoint> bigdata = ec.getBigData();
		double[] nnThresholds = ec.getNnInfo(totalTrue);
		if (queries.size() != nnThresholds.length) {
			throw new RuntimeException("hacky query size is wrong");
		}

		int count = 0;
		int trueCount = 0;

		long t0 = System.currentTimeMillis();
		int queryNo = 0;
		for (CartesianPoint p : queries) {
			double localThreshold =  nnThresholds[queryNo];
			double[] dists = new double[pivs.size()];
//			float[] dists = new float[pivs.size()];
			for (int i = 0; i < pivs.size(); i++) {
				dists[i] =  ec.metric.distance(p, pivs.get(i));
//				dists[i] =  (float) ec.metric.distance(p, pivs.get(i));
			}

			for (int i = 0; i < dataSize; i++) {
					
	
				double[] distObj = { se.dist1s[i], se.dist2s[i] };
				double[] distQuery= {dists[se.piv1s[i]],dists[se.piv2s[i]]};
				double pDist = pivDists[se.piv1s[i]][se.piv2s[i]];
				double obj_x_offset =  (distObj[0] * distObj[0] + pDist * pDist - distObj[1] * distObj[1]) / (2 * pDist);
				double obj_y_offset = Math.sqrt(distObj[0] * distObj[0] - obj_x_offset * obj_x_offset);
				double[] apex1 = { obj_x_offset, obj_y_offset };
				
				double q_x_offset =  (distQuery[0] * distQuery[0] + pDist * pDist - distQuery[1] * distQuery[1]) / (2 * pDist);
				double q_y_offset =  Math.sqrt(distQuery[0] * distQuery[0] - q_x_offset * q_x_offset);
				double[] apex2 = { q_x_offset, q_y_offset };
				
//				float[] apex1 = { se.dist1s[i], se.dist2s[i] };
//				float pDist = pivDists[se.piv1s[i]][se.piv2s[i]];
//				float d1 = dists[se.piv1s[i]];
//				float d2 = dists[se.piv2s[i]];
//
//				float x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
//				float y_offset = (float) Math.sqrt(d1 * d1 - x_offset * x_offset);
//				float[] apex2 = { x_offset, y_offset };
				double lwb = l2_2dim(apex1, apex2);
				if (checkDists && lwb <= localThreshold) { //<=
					final double trueD =  ec.metric.distance(p, bigdata.get(i));
					// count is the number of data which can't be excluded from the query
					count++;
				//	countSingleQuery++;
					if (trueD <= localThreshold) {
						// trueCount is the number which can't be excluded, but are correct solutions
						trueCount++;
					}
					// thus count - trueCount is the number of false negatives
				}
			}
			queryNo++;
		}
		long t1 = System.currentTimeMillis();

		// filter threshold nb this is not necessarily the same as the query
		// threshold... but we're about to remove that anyway!
		// so it's no longer here!


		
		System.out.print(pivSelectionMethod+"\t");
		// Exclusion Rate
		// For a single query: ExclusionRate= excludedObj/ (DatseSize-GTResults)
		// For all the queies (micro stat) ExclusionRate= excludedObjOverAlltheQueries/ (DatasetSize-GTResults)*#queries
		//excludedObjOvelAlltheQueries=DatasetSize**#queries -#NotExcluded=DatasetSize**#queries -count
		double microExclusionRate= (dataSize-(count/(double) queries.size()))/( dataSize-totalTrue);
		System.out.print(microExclusionRate +"\t");		
		
		// no of false negatives as a proportion of the total number of distances
		// required for exhaustive search
		System.out.print(1 - ((float) (count - trueCount) / (queries.size() * dataSize - totalTrue)));
		System.out.print("\t");
		// proportion of true positives found,presumably used as a check...
		System.out.print((float) trueCount / (totalTrue*queries.size()) );//LUCIA 
		System.out.print("\t");
		// time to execute in milliseconds
		System.out.print(t1 - t0);
		System.out.print("\t");
		// no of non-exclusions per query
		System.out.print(count / queries.size());
		System.out.println();
	}

	
	public static double l2_2dim(float[] a, float[] b) {
		float a0 = a[0] - b[0];
		float a1 = a[1] - b[1];

		return Math.sqrt(a0 * a0 + a1 * a1);
	}

	public static double l2_2dim(double[] a, double[] b) {
		double a0 = a[0] - b[0];
		double a1 = a[1] - b[1];

		return Math.sqrt(a0 * a0 + a1 * a1);
	}
}
