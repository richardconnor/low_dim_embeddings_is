package extreme_fourpoint;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.similarity.msc.core_concepts.CountedMetric;
import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.data.GistMetricSpace;
import eu.similarity.msc.data.SiftMetricSpace;
import eu.similarity.msc.data.cartesian.CartesianPoint;
import eu.similarity.msc.metrics.cartesian.Euclidean;
import eu.similarity.msc.metrics.floats.JensenShannon;
import eu.similarity.msc.search.VPTree;
import eu.similarity.msc.util.ObjectWithDistance;
import eu.similarity.msc.util.OrderedList;
import eu.similarity.msc.util.Quicksort;
import extreme_fourpoint.RefPointsSelector.DataPoint;

public class ExperimentsForIS {

	private static class Results {
		public int noOfResults;
		public float proportionAccess;
		public float queryTime;
	}

	public static Log log = LogFactory.getLog(ExperimentsForIS.class);

	public static DataSets[] dataSets = { DataSets.nasa, DataSets.colors, DataSets.euc10, DataSets.euc20, DataSets.sift,
			DataSets.gist };

	public static String rootDir = "/Volumes/Data/superExtreme/testData/";
	public static String SISAP_FILES_FOLDER = "/Volumes/Data/SISAP databases/dbs";
	public static String COLORS_FILE = SISAP_FILES_FOLDER + "/vectors/colors/colors.ascii";
	public static String NASA_FILE = SISAP_FILES_FOLDER + "/vectors/nasa/nasa.ascii";
	public static String YFCC_DATA_FILE = "/docsNotOnIcloud/Research/yfcc/data1_20k.obj";

	public static void main(String[] args) throws FileNotFoundException, Exception {
//		getIdims();
//		generateFullSiftGistData(DataSets.gist, getJsDistance());
//		log.info("started");
//		Map<Integer, CartesianPoint> x = ExperimentalContext_IS_paper.getFullData("sift");
//		System.out.println(x.size());
//		initialiseTestData();
//		initialiseFullData();
//		createNNFiles();
//		long t = generateFullSiftData();
//		System.out.println("that took: " + t / (float) (60 * 1000) + " minutes");

//		runFullSiftExperiment();
//		runFullGistExperiment();
//		runBaselineSiftExperiment();
//		printSerialSearchTimes();
//		printVptSearchTimes();
//		printSerialSearchTimes();
//		printVptSearchTimes();
//		runExperiment();
		runPivotTimesExperiment();
//		Metric<CartesianPoint> jsd = new JensenShannon<>(); 
		/*
		 * TODO above doesn't work for some reason MUST investigate!!
		 */
//		ExperimentalContext_IS_paper ec = new ExperimentalContext_IS_paper(DataSets.gist, getJsDistance(), 150);
//		findPivotDistribution(ec);

//		log.info("ended");
	}

	@SuppressWarnings("boxing")
	private static void runExperiment() throws Exception {
		Metric<CartesianPoint> jsd = getJsDistance();
		Metric<CartesianPoint> euc = new Euclidean<>();

//		int[] refPointNumbersForExperiment = { 2, 5, 10, 25, 50, 75, 100 };
		float[] shrinkageForExperiment = { 1, .9f, .8f, .7f, .6f, .5f, .4f, .3f, .2f, .1f };

//		final DataSets dataSet = DataSets.sift;
//		Metric<CartesianPoint> metric = euc;

//		final float shrinkFactor = 1f;
		final int noOfRefPoints = 100;
		final int noOfPivots = -1;

		for (DataSets dataSet : dataSets) {
			System.out.print("\t" + dataSet);
		}
		System.out.println();
		for (float shrinkFactor : shrinkageForExperiment) {
			System.out.print(shrinkFactor);
			for (DataSets dataSet : dataSets) {
				Metric<CartesianPoint> metric = (dataSet == DataSets.gist) ? jsd : euc;
				ExperimentalContext_IS_paper ec = new ExperimentalContext_IS_paper(dataSet, metric, noOfRefPoints);
//				ec.generatePivotTableData();
//				long buildTime = generateSurrogateData(ec);
//				long buildTime = generateRestrictedPivotData(ec, noOfPivots);
				Results res = doSurrogateQueries(ec, shrinkFactor, noOfPivots);
				System.out.print("\t" + res.queryTime + "\t" + res.noOfResults);
//				doSurrogateQueriesByRank(ec, 250, noOfPivots);
			}
			System.out.println();
		}
	}

	@SuppressWarnings("boxing")
	private static void runPivotTimesExperiment() throws Exception {

		int[] refPointNumbersForExperiment = { 2, 5, 10, 25, 50, 75, 100 };

		final float shrinkFactor = 1f;
		final int noOfPivots = -1;

		for (DataSets dataSet : dataSets) {
			System.out.print("\t" + dataSet);
		}
		System.out.println();
		for (int noOfRefPoints : refPointNumbersForExperiment) {
			System.out.print(noOfRefPoints);
			for (DataSets dataSet : dataSets) {
				ExperimentalContext_IS_paper ec = getContext(dataSet, noOfRefPoints);
				Results res = doSurrogateQueries(ec, shrinkFactor, noOfPivots);
				System.out.print("\t" + res.queryTime);
			}
			System.out.println();
		}

	}

	@SuppressWarnings("boxing")
	private static void runShrinkageExperiment() throws Exception {
		float[] shrinkageForExperiment = { 1, .9f, .8f, .7f, .6f, .5f, .4f, .3f, .2f, .1f };
		final int noOfRefPoints = 100;
		final int noOfPivots = -1;

		for (DataSets dataSet : dataSets) {
			System.out.print("\t" + dataSet);
		}
		System.out.println();
		for (float shrinkFactor : shrinkageForExperiment) {
			System.out.print(shrinkFactor);
			for (DataSets dataSet : dataSets) {
				ExperimentalContext_IS_paper ec = getContext(dataSet, noOfRefPoints);
				Results res = doSurrogateQueries(ec, shrinkFactor, noOfPivots);
				System.out.print("\t" + res.queryTime + "\t" + res.noOfResults);
			}
			System.out.println();
		}
	}

	public static Metric<CartesianPoint> getJsDistance() {
		Metric<float[]> jsd = new JensenShannon();
		Metric<CartesianPoint> jsdc = new Metric<CartesianPoint>() {

			@Override
			public double distance(CartesianPoint x, CartesianPoint y) {
				float[] p1 = getFloats(x.getPoint());
				float[] p2 = getFloats(y.getPoint());
				return jsd.distance(p1, p2);
			}

			private float[] getFloats(double[] point) {
				float[] res = new float[point.length];
				for (int i = 0; i < point.length; i++) {
					res[i] = (float) point[i];
				}
				return res;
			}

			@Override
			public String getMetricName() {
				return "jsdc";
			}
		};
		return jsdc;
	}

	@SuppressWarnings("unused")
	private static void createNNFiles() throws Exception {
//		ExperimentalContext_IS_paper.createNNinfo("nasa", 10);
//		ExperimentalContext_IS_paper.createNNinfo("colors", 10);
//		ExperimentalContext_IS_paper.createNNinfo("euc10", 10);
//		ExperimentalContext_IS_paper.createNNinfo("euc20", 10);
//		ExperimentalContext_IS_paper.createNNinfo("sift", 10);
		ExperimentalContext_IS_paper.createNNinfo("gist", getJsDistance(), 10);
	}

	@SuppressWarnings("unused")
	private static void initialiseTestData() throws FileNotFoundException, Exception {
//		ExperimentalContext_IS_paper.setUpData(DataSets.colors);
//		ExperimentalContext_IS_paper.setUpData(DataSets.nasa);
//		ExperimentalContext_IS_paper.setUpData(DataSets.euc10);
//		ExperimentalContext_IS_paper.setUpData(DataSets.euc20);
//		ExperimentalContext_IS_paper.setUpData(DataSets.sift);
//		ExperimentalContext_IS_paper.setUpData(DataSets.gist);

	}

	@SuppressWarnings("unused")
	private static void getIdims() throws FileNotFoundException, Exception {
		Metric<CartesianPoint> euc = new Euclidean<>();
		Metric<CartesianPoint> jsd = getJsDistance();
		getIdim(DataSets.gist, jsd);
	}

	private static void initialiseFullData() throws FileNotFoundException, Exception {
//		ExperimentalContext_IS_paper.setUpFullData(DataSets.gist);
//		ExperimentalContext_IS_paper.setUpFullData(DataSets.sift);
	}

	/**
	 * generates the XY coordinate data for the given data set, based on
	 * 
	 * 
	 * @param ec     this is used to get pivots, witnesses, data
	 * @param serial
	 * @throws Exception
	 */
	@SuppressWarnings("boxing")
	private static long generateSurrogateData(ExperimentalContext_IS_paper ec) throws Exception {
		long t0 = 0, t1 = 0;
		String fName = ec.getSurDataFileName();
		/*
		 * this construction also generates the piv_ file, which is a mapping from pivot
		 * ids to how many data points map to each. It would be neater in princple to
		 * generate this separately from the data file, maybe I'll change that one
		 * day....!
		 */
		String pivInfoFileName = fName.replace("sur_", "piv_");

		File f = new File(fName);
		if (!f.exists()) {
			List<CartesianPoint> wits = ec.getWitnesses();
			List<CartesianPoint> dat = ec.getBigData();
			// moderated by ec, not all available pivots!
			List<CartesianPoint> pivs = ec.getPivots();

			/*
			 * from SISAP paper:
			 * 
			 * and settled on the strategy of picking the pair which gave the largest
			 * distance to the third-nearest 2D point
			 */

			FourPointPivotsChooser2<CartesianPoint> pivotsChooser = new FourPointPivotsChooser2<>(pivs, ec.getMetric(),
					3);
			pivotsChooser.setWitnesses(wits);

			StoredExclusions se = new StoredExclusions();
			Map<Integer, Integer> pivotIdMap = new HashMap<>();

			int size = dat.size();
			se.dist1s = new float[size];
			se.dist2s = new float[size];
			se.piv1s = new short[size];
			se.piv2s = new short[size];

			t0 = System.currentTimeMillis();
			for (int i = 0; i < dat.size(); i++) {
				if (i % 100 == 0) {
					// System.out.println("doing " + i);
				}
				CartesianPoint d = dat.get(i);
				DataPoint<CartesianPoint> x = pivotsChooser.getDataPoint(d);

				final int pid = x.pivotId;
				if (!pivotIdMap.containsKey(pid)) {
					pivotIdMap.put(pid, 1);
				} else {
					pivotIdMap.put(pid, pivotIdMap.get(pid) + 1);
				}

				se.dist1s[i] = (float) x.apex[0];
				se.dist2s[i] = (float) x.apex[1];
				se.piv1s[i] = (short) x.refs[0];
				se.piv2s[i] = (short) x.refs[1];
			}
			t1 = System.currentTimeMillis();

			final FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(se);
			oos.close();

			final FileOutputStream fos1 = new FileOutputStream(pivInfoFileName);
			ObjectOutputStream oos1 = new ObjectOutputStream(fos1);
			oos1.writeObject(pivotIdMap);
			oos1.close();

		} else {
			log.info("data file exists");
		}
		return (t1 - t0);
	}

	/**
	 * generates the XY coordinate data for the given data set, based on a pivot
	 * info file that has been written during the original generation. Should also
	 * factor out this written form which can be gleaned quickly from the surrogate
	 * data file
	 * 
	 * 
	 * @param ec
	 * @param serial
	 * @throws Exception
	 */
	@SuppressWarnings("boxing")
	private static long generateRestrictedPivotData(ExperimentalContext_IS_paper ec, int noOfPivots) throws Exception {
		long t0 = 0, t1 = 0;

		// the four-point data from a fixed number of pivots
		String surrogateDataFileName = ec.getSurDataFileName();

		// where the pivot usage info has already been written
		String pivInfoFileName = surrogateDataFileName.replace("sur_", "piv_");

		// where the new four-point data will be written
		String outFileInfo = surrogateDataFileName.replace("sur_", "sur_lim_".concat(noOfPivots + "_"));

		Set<Integer> bestPivs = new HashSet<>();
		final FileInputStream fos1 = new FileInputStream(pivInfoFileName);
		ObjectInputStream oos1 = new ObjectInputStream(fos1);
		@SuppressWarnings("unchecked")
		Map<Integer, Integer> pivIdMap = (Map<Integer, Integer>) oos1.readObject();
		oos1.close();

		OrderedList<Integer, Integer> ol = new OrderedList<>(noOfPivots);
		for (int i : pivIdMap.keySet()) {
			ol.add(i, -(pivIdMap.get(i)));
		}
		bestPivs.addAll(ol.getList());

		File f = new File(outFileInfo);
		if (!f.exists()) {
			List<CartesianPoint> wits = ec.getWitnesses();
			List<CartesianPoint> dat = ec.getBigData();
			// moderated by ec, not all available pivots!
			List<CartesianPoint> pivs = ec.getPivots();

			/*
			 * from SISAP paper:
			 * 
			 * and settled on the strategy of picking the pair which gave the largest
			 * distance to the third-nearest 2D point
			 */

			FourPointPivotsChooser2<CartesianPoint> pivotsChooser = new FourPointPivotsChooser2<>(pivs, ec.getMetric(),
					3);
			pivotsChooser.setBestPivots(bestPivs);
			pivotsChooser.setWitnesses(wits);

			StoredExclusions se = new StoredExclusions();
			Map<Integer, Integer> pivotIdMap = new HashMap<>();

			int size = dat.size();
			se.dist1s = new float[size];
			se.dist2s = new float[size];
			se.piv1s = new short[size];
			se.piv2s = new short[size];

			t0 = System.currentTimeMillis();
			for (int i = 0; i < dat.size(); i++) {
				if (i % 100 == 0) {
					// System.out.println("doing " + i);
				}
				CartesianPoint d = dat.get(i);
				DataPoint<CartesianPoint> x = pivotsChooser.getDataPoint(d);

				final int pid = x.pivotId;
				if (!pivotIdMap.containsKey(pid)) {
					pivotIdMap.put(pid, 1);
				} else {
					pivotIdMap.put(pid, pivotIdMap.get(pid) + 1);
				}

				se.dist1s[i] = (float) x.apex[0];
				se.dist2s[i] = (float) x.apex[1];
				se.piv1s[i] = (short) x.refs[0];
				se.piv2s[i] = (short) x.refs[1];
			}
			t1 = System.currentTimeMillis();

			final FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(se);
			oos.close();
		} else {
			log.info("resticted pivot data file already exists");
		}
		return (t1 - t0);
	}

	private static Map<Integer, Exclusion> getFullSiftGistData(DataSets dataSet, Metric<CartesianPoint> metric)
			throws IOException, ClassNotFoundException {

		ExperimentalContext_IS_paper ec = new ExperimentalContext_IS_paper(dataSet, metric, 250);
		int noOfPivots = 1000;
		String surrogateDataFileName = ec.getSurDataFileName();
		String dataFileGenericName = surrogateDataFileName
				.replace("/sur_", "/full_data/sur_full_lim_".concat(noOfPivots + "_")).replace(".dat", "_XXX.dat");
		Map<Integer, Exclusion> res = new HashMap<>();
		for (int i = 0; i < 1000; i++) {
			String dataFileName = dataFileGenericName.replace("XXX", i + "");
			File outputFile = new File(dataFileName);
			final FileInputStream fis = new FileInputStream(outputFile);
			ObjectInputStream ois = new ObjectInputStream(fis);
			@SuppressWarnings("unchecked")
			Map<Integer, Exclusion> map = (Map<Integer, Exclusion>) ois.readObject();
			res.putAll(map);
			ois.close();
		}
		return res;
	}

	/**
	 * generates the XY coordinate data for the given data set, based on a pivot
	 * info file that has been written during the original generation. Should also
	 * factor out this written form which can be gleaned quickly from the surrogate
	 * data file
	 * 
	 * 
	 * @param ec
	 * @param serial
	 * @throws Exception
	 */
	@SuppressWarnings("boxing")
	private static long generateFullSiftGistData(DataSets dataSet, Metric<CartesianPoint> metric) throws Exception {
		long t0 = 0, t1 = 0;

		ExperimentalContext_IS_paper ec = new ExperimentalContext_IS_paper(dataSet, metric, 250);
		int noOfPivots = 1000;
		// the four-point data from a fixed number of pivots; this file isn't going to
		// be used however, just its name... could change the get...Name function which
		// would be better.
		String surrogateDataFileName = ec.getSurDataFileName();

		// where the pivot usage info has already been written
		String pivInfoFileName = surrogateDataFileName.replace("sur_", "piv_");

		// where the new four-point data will be written
		String outFileName = surrogateDataFileName.replace("/sur_",
				"/full_data/sur_full_lim_".concat(noOfPivots + "_"));

		Set<Integer> bestPivs = new HashSet<>();
		final FileInputStream fos1 = new FileInputStream(pivInfoFileName);
		ObjectInputStream oos1 = new ObjectInputStream(fos1);
		@SuppressWarnings("unchecked")
		Map<Integer, Integer> pivIdMap = (Map<Integer, Integer>) oos1.readObject();
		oos1.close();

		OrderedList<Integer, Integer> ol = new OrderedList<>(noOfPivots);
		for (int i : pivIdMap.keySet()) {
			ol.add(i, -(pivIdMap.get(i)));
		}
		bestPivs.addAll(ol.getList());

		List<CartesianPoint> wits = ec.getWitnesses();
		Map<Integer, CartesianPoint> dat = ec.getFullData();
		// moderated by ec, not all available pivots!
		List<CartesianPoint> pivs = ec.getPivots();

		FourPointPivotsChooser2<CartesianPoint> pivotsChooser = new FourPointPivotsChooser2<>(pivs, ec.getMetric(), 3);
		pivotsChooser.setBestPivots(bestPivs);
		pivotsChooser.setWitnesses(wits);

		Map<Integer, Exclusion> se = new HashMap<>();

		int noDone = 0;
		t0 = System.currentTimeMillis();
		for (int dataId : dat.keySet()) {
			CartesianPoint d = dat.get(dataId);
			DataPoint<CartesianPoint> twoDpoint = pivotsChooser.getDataPoint(d);

			float x = (float) twoDpoint.apex[0];
			float y = (float) twoDpoint.apex[1];
			short id1 = (short) twoDpoint.refs[0];
			short id2 = (short) twoDpoint.refs[1];

			se.put(dataId, new Exclusion(id1, id2, x, y));
			noDone++;

			if (noDone % 1000 == 0) {

				String outputFileName = outFileName.replace(".dat", "_" + ((noDone / 1000) - 1) + ".dat");
				File outputFile = new File(outputFileName);
				final FileOutputStream fos = new FileOutputStream(outputFile);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(se);
				oos.close();
				se = new HashMap<>();

				log.info("done file " + ((noDone / 1000) - 1));
			}

		}
		t1 = System.currentTimeMillis();

		return (t1 - t0);
	}

	public static Results doSurrogateQueries(ExperimentalContext_IS_paper ec, float shrinkFactor, int noOfPivots)
			throws Exception {

		String surDataTable = ec.getSurDataFileName();
		if (noOfPivots > 0) {
			surDataTable = surDataTable.replace("sur_", "sur_lim_".concat(noOfPivots + "_"));
		}
		FileInputStream fis2 = new FileInputStream(surDataTable);
		ObjectInputStream ois2 = new ObjectInputStream(fis2);

		float[][] pivDists = ec.getPivotDistances();
		StoredExclusions se = (StoredExclusions) ois2.readObject();
		fis2.close();
		int dataSize = se.dist1s.length;

		List<CartesianPoint> queries = ec.getQueries();
		List<CartesianPoint> pivs = ec.getPivots();
		List<CartesianPoint> bigdata = ec.getBigData();
		double[] nnThresholds = ec.getNnInfo(1);
		if (queries.size() != nnThresholds.length) {
			throw new RuntimeException("hacky query size is wrong");
		}

		int count = 0;
		int trueCount = 0;

		long t0 = System.currentTimeMillis();
		int queryNo = 0;
		for (CartesianPoint q : queries) {
			double threshold = nnThresholds[queryNo];
			float[] dists = new float[pivs.size()];
			for (int i = 0; i < pivs.size(); i++) {
				dists[i] = (float) ec.getMetric().distance(q, pivs.get(i));
			}

			for (int i = 0; i < dataSize; i++) {
				float[] apex1 = { se.dist1s[i], se.dist2s[i] };
				float pDist = pivDists[se.piv1s[i]][se.piv2s[i]];
				float d1 = dists[se.piv1s[i]];
				float d2 = dists[se.piv2s[i]];

				float x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
				float y_offset = (float) Math.sqrt(d1 * d1 - x_offset * x_offset);
				float[] apex2 = { x_offset, y_offset };
				double lwb = l2_2dim(apex1, apex2);

				if (lwb <= threshold * shrinkFactor) {

					final double trueD = ec.getMetric().distance(q, bigdata.get(i));
					// count is the number of data which can't be excluded from the query
					count++;

					if (trueD <= threshold) {
						trueCount++;
					}
				}
			}
			queryNo++;
		}
		long t1 = System.currentTimeMillis();

		// return 3 values: proportion of data accessed per query; no of results; time
		// per query
		@SuppressWarnings("synthetic-access")
		Results res = new Results();
		res.noOfResults = trueCount;
		res.proportionAccess = (float) (count) / (queries.size() * dataSize);
		res.queryTime = ((float) (t1 - t0)) / queries.size();
		return res;
	}

	/**
	 * idea here is to take the smallest n distances according to lwb and return the
	 * smallest true distances
	 * 
	 * @param ec
	 * @param noToConsider
	 * @param noOfPivots
	 * @throws Exception
	 */
	public static void doSurrogateQueriesByRank(ExperimentalContext_IS_paper ec, int noToConsider, int noOfPivots)
			throws Exception {

		String surDataTable = ec.getSurDataFileName();
		if (noOfPivots > 0) {
			surDataTable = surDataTable.replace("sur_", "sur_lim_".concat(noOfPivots + "_"));
		}
		FileInputStream fis2 = new FileInputStream(surDataTable);
		ObjectInputStream ois2 = new ObjectInputStream(fis2);
		StoredExclusions se = (StoredExclusions) ois2.readObject();
		fis2.close();

		float[][] pivDists = ec.getPivotDistances();
		int dataSize = se.dist1s.length;

		List<CartesianPoint> queries = ec.getQueries();
		List<CartesianPoint> pivs = ec.getPivots();
		List<CartesianPoint> trueData = ec.getBigData();

		double[] nnThresholds = ec.getNnInfo(10);

		long t0 = System.currentTimeMillis();
		int queryNo = 0;
		double[] smalls = new double[queries.size()];

		for (CartesianPoint q : queries) {
			float[] queryToPivotDistances = new float[pivs.size()];
			for (int i = 0; i < pivs.size(); i++) {
				queryToPivotDistances[i] = (float) ec.getMetric().distance(q, pivs.get(i));
			}

			OrderedList<Integer, Double> smallestLwbs = new OrderedList<>(noToConsider);

			for (int i = 0; i < dataSize; i++) {
				float[] apex1 = { se.dist1s[i], se.dist2s[i] };
				float pDist = pivDists[se.piv1s[i]][se.piv2s[i]];
				float d1 = queryToPivotDistances[se.piv1s[i]];
				float d2 = queryToPivotDistances[se.piv2s[i]];

				float x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
				float y_offset = (float) Math.sqrt(d1 * d1 - x_offset * x_offset);
				float[] apex2 = { x_offset, y_offset };

				double lwb = l2_2dim(apex1, apex2);

				smallestLwbs.add(i, lwb);
			}
			double smallest = Double.MAX_VALUE;
			for (int dataId : smallestLwbs.getList()) {
				double trueD = ec.getMetric().distance(trueData.get(dataId), q);
				smallest = Math.min(smallest, trueD);
			}
			smalls[queryNo] = smallest;

			queryNo++;
		}
		long t1 = System.currentTimeMillis();

		int found = 0;
		int ptr = 0;
		for (double d : smalls) {
			if (d <= nnThresholds[ptr++]) {
				found++;
			}
		}
		System.out.println(found);
	}

	public static void findPivotDistribution(ExperimentalContext_IS_paper ec) throws Exception {
		String surDataTable = ec.getSurDataFileName();
		FileInputStream fis = new FileInputStream(surDataTable);
		ObjectInputStream ois = new ObjectInputStream(fis);
		StoredExclusions se = (StoredExclusions) ois.readObject();
		ois.close();

		int[][] matrix = new int[ec.getNoOfRefPoints()][ec.getNoOfRefPoints()];

		int dataSize = se.dist1s.length;
		for (int i = 0; i < dataSize; i++) {
			short p1 = se.piv1s[i];
			short p2 = se.piv2s[i];
			matrix[p1][p2] = matrix[p1][p2] + 1;
		}
		for (int x = 0; x < ec.getNoOfRefPoints(); x++) {
			for (int y = 0; y < ec.getNoOfRefPoints(); y++) {
				System.out.print(matrix[x][y] + "\t");
			}
			System.out.println();
		}
	}

	public static double l2_2dim(float[] a, float[] b) {
		float a0 = a[0] - b[0];
		float a1 = a[1] - b[1];

		return Math.sqrt(a0 * a0 + a1 * a1);
	}

	public static double l2(float[] a, float[] b) {
		double acc = 0;
		for (int i = 0; i < a.length; i++) {
			float diff = a[i] - b[i];
			acc = acc + diff * diff;
		}
		return Math.sqrt(acc);
	}

	public static double zen_2dim(float[] a, float[] b) {

		return Math.sqrt(Math.abs(a[0] - b[0]) * Math.abs(a[0] - b[0]) + a[1] * a[1] + b[1] * b[1]);

	}

	public static double inflate(float[] a, float[] b, double theta) {
		float t1 = b[1];
		double t3 = Math.sqrt(Math.abs(a[0] - b[0]) * Math.abs(a[0] - b[0]) + a[1] * a[1]);
		return Math.sqrt(t1 * t1 + t3 * t3 - 2 * t1 * t3 * Math.cos((theta / 180) * Math.PI));
	}

	private static double getAngle(double t1, double t2, double t3) {
		double theta = Math.acos((t1 * t1 + t3 * t3 - t2 * t2) / (2 * t1 * t3));
		final double degrees = (theta / Math.PI) * 180;
		return degrees;
	}

	@SuppressWarnings("boxing")
	private static void runFullSiftExperiment() throws Exception {
		Metric<CartesianPoint> euc = new Euclidean<>();
		final DataSets dataSet = DataSets.sift;

		Map<Integer, Exclusion> fps = getFullSiftGistData(dataSet, euc);
		SiftMetricSpace siftSpace = new SiftMetricSpace("/Volumes/Data/SIFT_mu/");
		Map<Integer, float[]> queries = siftSpace.getQueries();

		Map<Integer, List<Integer>> nnids = siftSpace.getNNIds();
		Map<Integer, Set<Integer>> nnidSets = new HashMap<>();
		for (int i : nnids.keySet()) {
			Set<Integer> s = new HashSet<>();
			for (int id : nnids.get(i)) {
				s.add(id);
			}
			nnidSets.put(i, s);
		}

		ExperimentalContext_IS_paper ec = new ExperimentalContext_IS_paper(dataSet, euc, 250);
		float[][] pivDists = ec.getPivotDistances();
		List<CartesianPoint> pivs = ec.getPivots();

		for (int queryId : queries.keySet()) {
			CartesianPoint query = new CartesianPoint(queries.get(queryId));
			float[] queryToPivotDistances = new float[pivs.size()];
			for (int i = 0; i < pivs.size(); i++) {
				final float d1 = (float) euc.distance(query, pivs.get(i));
				queryToPivotDistances[i] = d1;
			}

			@SuppressWarnings("unchecked")
			ObjectWithDistance<Integer>[] arr = new ObjectWithDistance[fps.size()];
//			OrderedList<Integer, Double> smallestLwbs = new OrderedList<>(10000);
			int ptr = 0;
			for (int dataId : fps.keySet()) {
				Exclusion fourPointInfo = fps.get(dataId);

				float[] apex1 = { (float) fourPointInfo.getX(), (float) fourPointInfo.getY() };
				float pDist = pivDists[fourPointInfo.getRef1()][fourPointInfo.getRef2()];
				float d1 = queryToPivotDistances[fourPointInfo.getRef1()];
				float d2 = queryToPivotDistances[fourPointInfo.getRef2()];

				float x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
				float y_offset = (float) Math.sqrt(d1 * d1 - x_offset * x_offset);
				float[] apex2 = { x_offset, y_offset };

				double lwb = l2_2dim(apex1, apex2);
				double zen = zen_2dim(apex1, apex2);

				arr[ptr++] = new ObjectWithDistance<>(dataId, lwb);
			}

			Quicksort.sort(arr);

			int[] noFoundAtOrder = new int[10];
			for (int percent = 10; percent > 0; percent--) {
				int number = 10000 * percent;
				int noFound = 0;
				final Set<Integer> queryNNids = nnidSets.get(queryId);
				for (int i = 0; i < number; i++) {
//				final double trueDist = l2(queries.get(queryId), originalData.get(arr[i].getValue()));
					if (queryNNids.contains(arr[i].getValue())) {
						noFound++;
					}
				}
				noFoundAtOrder[percent - 1] = noFound;
			}
			System.out.print(queryId);
			for (int no : noFoundAtOrder) {
				System.out.print("\t" + no);
			}
			System.out.println();
		}
		long t1 = System.currentTimeMillis();
	}

	@SuppressWarnings("boxing")
	private static void runFullGistExperiment() throws Exception {

		Metric<CartesianPoint> jsd = getJsDistance();
		final DataSets dataSet = DataSets.gist;
		Map<Integer, Exclusion> fps = getFullSiftGistData(dataSet, jsd);

		GistMetricSpace ggd = new GistMetricSpace("/Volumes/Data/mf_gist/");
		ExperimentalContext_IS_paper ec = new ExperimentalContext_IS_paper(dataSet, jsd, 250);

		List<CartesianPoint> queries = ec.getQueries();
		Map<Integer, float[]> originalData = ggd.getData();
		Map<Integer, List<Integer>> nnids = ggd.getNNIds();
		Map<Integer, Set<Integer>> nnidSets = new HashMap<>();
		for (int i : nnids.keySet()) {
			Set<Integer> s = new HashSet<>();
			for (int id : nnids.get(i)) {
				s.add(id);
			}
			nnidSets.put(i, s);
		}

		float[][] pivDists = ec.getPivotDistances();
		List<CartesianPoint> pivs = ec.getPivots();

		for (int queryId : nnids.keySet()) {
			float[] q = originalData.get(queryId);
			CartesianPoint query = new CartesianPoint(q);
			float[] queryToPivotDistances = new float[pivs.size()];
			for (int i = 0; i < pivs.size(); i++) {
				final float d1 = (float) jsd.distance(query, pivs.get(i));
				queryToPivotDistances[i] = d1;
			}

			@SuppressWarnings("unchecked")
			ObjectWithDistance<Integer>[] arr = new ObjectWithDistance[fps.size()];
//			OrderedList<Integer, Double> smallestLwbs = new OrderedList<>(10000);
			int ptr = 0;
			for (int dataId : fps.keySet()) {
				Exclusion fourPointInfo = fps.get(dataId);

				float[] apex1 = { (float) fourPointInfo.getX(), (float) fourPointInfo.getY() };
				float pDist = pivDists[fourPointInfo.getRef1()][fourPointInfo.getRef2()];
				float d1 = queryToPivotDistances[fourPointInfo.getRef1()];
				float d2 = queryToPivotDistances[fourPointInfo.getRef2()];

				float x_offset = (d1 * d1 + pDist * pDist - d2 * d2) / (2 * pDist);
				float y_offset = (float) Math.sqrt(d1 * d1 - x_offset * x_offset);
				float[] apex2 = { x_offset, y_offset };

				double lwb = l2_2dim(apex1, apex2);
//				double zen = zen_2dim(apex1, apex2);

				arr[ptr++] = new ObjectWithDistance<>(dataId, lwb);
			}

			Quicksort.sort(arr);

			int[] noFoundAtOrder = new int[10];
			for (int percent = 10; percent > 0; percent--) {
				int number = 10000 * percent;
				int noFound = 0;
				final Set<Integer> queryNNids = nnidSets.get(queryId);
				for (int i = 0; i < number; i++) {
//				final double trueDist = l2(queries.get(queryId), originalData.get(arr[i].getValue()));
					if (queryNNids.contains(arr[i].getValue())) {
						noFound++;
					}
				}
				noFoundAtOrder[percent - 1] = noFound;
			}
			System.out.print(queryId);
			for (int no : noFoundAtOrder) {
				System.out.print("\t" + no);
			}
			System.out.println();
		}
		long t1 = System.currentTimeMillis();
	}

	private static void runBaselineSiftExperiment() throws Exception {
		SiftMetricSpace gds = new SiftMetricSpace("/Volumes/Data/SIFT_mu/");
		Map<Integer, float[]> queries = gds.getQueries();
		Map<Integer, float[]> originalData = gds.getData();

		Map<Integer, List<Integer>> nnids = gds.getNNIds();

		for (int qid : queries.keySet()) {
			long t0 = System.currentTimeMillis();
			float[] query = queries.get(qid);
			OrderedList<Integer, Double> results = new OrderedList<>(100);
			for (int did : originalData.keySet()) {
				float[] datum = originalData.get(did);
				results.add(did, l2(query, datum));
			}
			long t1 = System.currentTimeMillis();
			System.out.println("query " + qid + " took " + (t1 - t0) + " msecs : " + results.getList().get(0));
		}
	}

	private static void getIdim(DataSets dataSet, Metric<CartesianPoint> metric) throws Exception {
		ExperimentalContext_IS_paper ec = new ExperimentalContext_IS_paper(dataSet, metric, 2);
		List<CartesianPoint> data = ec.getBigData();
		Random rand = new Random(0);
		for (int t = 0; t < 100; t++) {
			for (int i = 0; i < 100; i++) {
				double d = metric.distance(data.get(rand.nextInt(data.size())), data.get(rand.nextInt(data.size())));
				System.out.print((float) d + "\t");
			}
			System.out.println();
		}
	}

	private static ExperimentalContext_IS_paper getContext(DataSets dataSet, int noOfRefPoints) {
		Metric<CartesianPoint> jsd = getJsDistance();
		Metric<CartesianPoint> euc = new Euclidean<>();
		Metric<CartesianPoint> metric = (dataSet == DataSets.gist) ? jsd : euc;
		return new ExperimentalContext_IS_paper(dataSet, metric, noOfRefPoints);
	}

	private static void printSerialSearchTimes() throws Exception {
		System.out.println("serial search, times per query");
		for (DataSets dataSet : dataSets) {
			System.out.print(dataSet + "\t");
		}
		System.out.println();
		for (DataSets dataSet : dataSets) {
			ExperimentalContext_IS_paper ec = getContext(dataSet, 100);
			List<CartesianPoint> data = ec.getBigData();
			List<CartesianPoint> queries = ec.getQueries();
			final Metric<CartesianPoint> metric = ec.getMetric();
			long t0 = System.currentTimeMillis();
			for (CartesianPoint q : queries) {
				for (CartesianPoint d : data) {
					metric.distance(q, d);
				}
			}
			long t1 = System.currentTimeMillis();
			System.out.print((float) (t1 - t0) / (float) queries.size() + "\t");
		}
		System.out.println();
	}

	private static void printVptSearchTimes() throws Exception {
		System.out.println("vpt search, times per query");
		for (DataSets dataSet : dataSets) {
			System.out.print(dataSet + "\t");
		}
		System.out.println();
		for (DataSets dataSet : dataSets) {
			ExperimentalContext_IS_paper ec = getContext(dataSet, 100);
			List<CartesianPoint> data = ec.getBigData();
			List<CartesianPoint> queries = ec.getQueries();
			double[] nnThresholds = ec.getNnInfo(1);
			CountedMetric<CartesianPoint> cm = new CountedMetric<>(ec.getMetric());
			VPTree<CartesianPoint> vpt = new VPTree<>(data, cm);
			cm.reset();
			long t0 = System.currentTimeMillis();
			int threshPtr = 0;
			for (CartesianPoint q : queries) {
				double thresh = nnThresholds[threshPtr++];
				List<CartesianPoint> res = vpt.search(q, thresh);
			}
			long t1 = System.currentTimeMillis();
//			System.out.print((float) (t1 - t0) / (float) queries.size() + "\t");
			System.out.print((float) cm.reset() / (float) (queries.size() * data.size()) + "\t");
		}
		System.out.println();
	}

}
