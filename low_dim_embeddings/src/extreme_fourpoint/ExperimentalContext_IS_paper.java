package extreme_fourpoint;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.data.GistMetricSpace;
import eu.similarity.msc.data.SiftMetricSpace;
import eu.similarity.msc.data.cartesian.CartesianPoint;
import eu.similarity.msc.data.testloads.CartesianPointGenerator;
import eu.similarity.msc.data.testloads.TestLoad;
import eu.similarity.msc.data.testloads.TestLoad.SisapFile;
import eu.similarity.msc.metrics.cartesian.Euclidean;
import eu.similarity.msc.search.VPTree;
import eu.similarity.msc.util.CartesianPointFileReader;

public class ExperimentalContext_IS_paper {
	private static Random rand = new Random(0);
	private static Log log = LogFactory.getLog(ExperimentalContext_IS_paper.class);

	public static void createTestData(List<CartesianPoint> data, String dataName)
			throws Exception, FileNotFoundException {

		File f = new File(ExperimentsForIS.rootDir + dataName);
		if (!f.exists()) {
			f.mkdir();
		}

		printDataFile(data, dataName, "/pivots", 3000);
		printDataFile(data, dataName, "/witness", 5000);
		printDataFile(data, dataName, "/testdata", 1000);
		printDataFile(data, dataName, "/queries", 1000);
		printDataFile(data, dataName, "/bigdata", 10000);
	}

	public static void createFullData(Map<Integer, CartesianPoint> data, String dataName)
			throws Exception, FileNotFoundException {

		File f = new File(ExperimentsForIS.rootDir + dataName + "/fulldata.obj");
		if (!f.exists()) {
			OutputStream os = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(data);
			oos.close();
		} else {
			log.fatal(dataName + "fulldata.obj already exists");
		}
	}

	public Map<Integer, CartesianPoint> getFullData() throws Exception, FileNotFoundException {
		File f = new File(ExperimentsForIS.rootDir + this.dataset + "/fulldata.obj");
		if (f.exists()) {
			InputStream os = new FileInputStream(f);
			ObjectInputStream oos = new ObjectInputStream(os);
			@SuppressWarnings("unchecked")
			Map<Integer, CartesianPoint> res = (Map<Integer, CartesianPoint>) oos.readObject();
			oos.close();
			return res;
		} else {
			log.fatal(this.dataset + "fulldata.obj doesn't exist");
			return null;
		}
	}

	/**
	 * create new NN file for the given dataset, format is: queryid: threshold for
	 * noOfNN.
	 * 
	 * @param dataName
	 * @param noOfNN
	 * @throws Exception
	 */
	public static void createNNinfo(String dataName, Metric<CartesianPoint> metric, int noOfNN) throws Exception {

		CartesianPointFileReader queries = new CartesianPointFileReader(
				ExperimentsForIS.rootDir + dataName + "/queries.dat", false);
		CartesianPointFileReader data = new CartesianPointFileReader(
				ExperimentsForIS.rootDir + dataName + "/bigdata.dat", false);

		VPTree<CartesianPoint> vpt = new VPTree<>(data, metric);

		PrintWriter pw = new PrintWriter(ExperimentsForIS.rootDir + dataName + "/nnInfo.dat");

		int ptr = 0;
		for (CartesianPoint q : queries) {
			List<Integer> res = vpt.nearestNeighbour(q, noOfNN);
			pw.print(ptr++ + "\t");
			for (int i : res) {
				pw.print("\t" + metric.distance(q, data.get(i)));
			}
			pw.println();

		}
		pw.close();
	}

	public static double[] getNNinfo(String dataName, int whichNN) throws Exception {
		LineNumberReader lnr = new LineNumberReader(
				new FileReader(ExperimentsForIS.rootDir + dataName + "/nnInfo.dat"));
		double[] res = new double[1000];
		int ptr = 0;
		for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
			Scanner s = new Scanner(line);
			s.nextInt(); // strip id as they're currently all in order from 0
			for (int i = 0; i < whichNN; i++) {
				res[ptr] = s.nextDouble();
			}
			s.close();
			ptr++;
		}

		lnr.close();
		return res;
	}

	public static void setUpData(DataSets expData) throws Exception, FileNotFoundException {
		switch (expData) {
		case colors: {
			List<CartesianPoint> data = (new TestLoad(SisapFile.colors)).getData();
			createTestData(data, "colors");

			break;
		}
		case nasa: {
			List<CartesianPoint> data = (new TestLoad(SisapFile.nasa)).getData();
			createTestData(data, "nasa");

			break;
		}
		case euc10: {
			CartesianPointGenerator cg = new CartesianPointGenerator(10, false);
			List<CartesianPoint> data = new ArrayList<>();
			for (int i = 0; i < 20000; i++) {
				data.add(cg.next());
			}
			createTestData(data, "euc10");

			break;
		}
		case euc20: {
			CartesianPointGenerator cg = new CartesianPointGenerator(20, false);
			List<CartesianPoint> data = new ArrayList<>();
			for (int i = 0; i < 20000; i++) {
				data.add(cg.next());
			}
			createTestData(data, "euc20");

			break;
		}
		case sift: {
			String path = "/Volumes/Data/SIFT_mu/";
			SiftMetricSpace gds = new SiftMetricSpace(path);
			Map<Integer, float[]> sifts = gds.getData(20);

			List<CartesianPoint> data = new ArrayList<>();
			for (int i : sifts.keySet()) {
				float[] arr = sifts.get(i);
				data.add(new CartesianPoint(arr));
			}

			createTestData(data, "sift");

			break;
		}
		case gist: {
			String path = "/Volumes/Data/mf_gist/";
			GistMetricSpace ggd = new GistMetricSpace(path);
			Map<Integer, float[]> gists = ggd.getData(20);

			List<CartesianPoint> data = new ArrayList<>();
			for (int i : gists.keySet()) {
				float[] arr = gists.get(i);
				data.add(new CartesianPoint(arr));
			}

			createTestData(data, "gist");

			break;
		}
		case alexnet: {
			Map<Integer, float[]> map = MFExperiments.getRawFile(0);
			List<CartesianPoint> data = new ArrayList<>();
			for (float[] fs : map.values()) {
				CartesianPoint cp = new CartesianPoint(fs);
				data.add(cp);
			}

			break;
		}
		case yfcc: {
			FileInputStream fis = new FileInputStream(ExperimentsForIS.YFCC_DATA_FILE);
			ObjectInputStream ois = new ObjectInputStream(fis);
			float[][] dat = (float[][]) ois.readObject();
			ois.close();
			List<CartesianPoint> data = new ArrayList<>();
			for (float[] floats : dat) {
				CartesianPoint cp = new CartesianPoint(floats);
				data.add(cp);
			}
			createTestData(data, "yfcc");

			break;
		}
		default: {
			log.fatal("setUpData: no initialisation defined for " + expData);
		}
		}
	}

	public static void setUpFullData(DataSets expData) throws Exception, FileNotFoundException {
		switch (expData) {

		case sift: {
			String path = "/Volumes/Data/SIFT_mu/";
			SiftMetricSpace gds = new SiftMetricSpace(path);
			Map<Integer, float[]> sifts = gds.getData();

			Map<Integer, CartesianPoint> data = new HashMap<>();
			for (int i : sifts.keySet()) {
				float[] arr = sifts.get(i);
				data.put(i, new CartesianPoint(arr));
			}

			createFullData(data, "sift");

			break;
		}
		case gist: {
			String path = "/Volumes/Data/mf_gist/";
			GistMetricSpace ggd = new GistMetricSpace(path);
			Map<Integer, float[]> gists = ggd.getData();

			Map<Integer, CartesianPoint> data = new HashMap<>();
			for (int i : gists.keySet()) {
				float[] arr = gists.get(i);
				data.put(i, new CartesianPoint(arr));
			}

			createFullData(data, "gist");

			break;
		}

		default: {
			log.fatal("setUpData: no initialisation defined for " + expData);
		}
		}
	}

	private static void printDataFile(List<CartesianPoint> data, final String dataSet, final String fileType,
			final int number) throws FileNotFoundException {

		PrintWriter pw = new PrintWriter(ExperimentsForIS.rootDir + dataSet + fileType + ".dat");

		for (int i = 0; i < number; i++) {
			CartesianPoint p = data.remove(rand.nextInt(data.size()));
			for (double d : p.getPoint()) {
				pw.print(d + "\t");
			}
			pw.println();
		}
		pw.close();
	}

	private int noOfRefPoints;
	private int noOfWitnessData;
	private DataSets dataset;
	private int k;
	private Metric<CartesianPoint> metric;

	ExperimentalContext_IS_paper(DataSets data, int noOfRefPoints) {
		this.noOfRefPoints = noOfRefPoints;
		this.dataset = data;
		this.noOfWitnessData = 1000;
		this.k = 3;
		this.metric = new Euclidean<>();
	}

	ExperimentalContext_IS_paper(DataSets data, Metric<CartesianPoint> metric, int noOfRefPoints) {
		this.noOfRefPoints = noOfRefPoints;
		this.dataset = data;
		this.noOfWitnessData = 1000;
		this.k = 3;
		this.metric = metric;
	}

	public void generatePivotTableData() throws Exception {
		final String filename = ExperimentsForIS.rootDir + this.dataset + "/surrogates/pivotDists"
				+ this.getNoOfRefPoints() + "_euc" + ".dat";

		File f = new File(filename);
		if (!f.exists()) {
			List<CartesianPoint> pivs = this.getPivots();

			FourPointPivotsChooser<CartesianPoint> pc = new FourPointPivotsChooser<>(pivs, this.getMetric(), 0, false,
					0);

			pc.writePivotDistanceTable(filename);
		} else {
			log.info("pivot file exists");
		}

	}

	public List<CartesianPoint> getBigData() throws Exception {
		return new CartesianPointFileReader(ExperimentsForIS.rootDir + this.dataset + "/bigdata.dat", false);
	}

	public float[][] getPivotDistances() throws ClassNotFoundException, IOException {

		String pivotDistTable = ExperimentsForIS.rootDir + this.dataset + "/surrogates/pivotDists"
				+ this.getNoOfRefPoints() + "_euc" + ".dat";
		FileInputStream fis1 = new FileInputStream(pivotDistTable);
		ObjectInputStream ois1 = new ObjectInputStream(fis1);
		final float[][] dists = (float[][]) ois1.readObject();
		ois1.close();
		return dists;
	}

	public List<CartesianPoint> getPivots() throws Exception {
		return new CartesianPointFileReader(ExperimentsForIS.rootDir + this.dataset + "/pivots.dat", false).subList(0,
				this.getNoOfRefPoints());
	}

	public double[] getNnInfo(int whichNN) throws Exception {
		LineNumberReader lnr = new LineNumberReader(
				new FileReader(ExperimentsForIS.rootDir + this.dataset + "/nnInfo.dat"));
		double[] res = new double[1000];
		int ptr = 0;
		for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
			Scanner s = new Scanner(line);
			s.nextInt(); // strip id as they're currently all in order from 0
			for (int i = 0; i < whichNN; i++) {
				res[ptr] = s.nextDouble();
			}
			s.close();
			ptr++;
		}

		lnr.close();
		return res;
	}

	public List<CartesianPoint> getQueries() throws Exception {
		return new CartesianPointFileReader(ExperimentsForIS.rootDir + this.dataset + "/queries.dat", false);
	}

	public String getSurDataFileName() {
		final String dirName = ExperimentsForIS.rootDir + dataset + "/surrogates";
		File f = new File(dirName);
		if (!f.exists()) {
			f.mkdir();
		}
		final String fname = dirName + "/sur_" + this.getNoOfRefPoints() + "_" + this.noOfWitnessData + "_"
				+ this.getMetric().getMetricName() + "_nn" + this.k + ".dat";
		return fname;
	}

	public List<CartesianPoint> getTestData() throws Exception {
		return new CartesianPointFileReader(ExperimentsForIS.rootDir + this.dataset + "/testdata.dat", false);
	}

	public List<CartesianPoint> getWitnesses() throws Exception {
		return new CartesianPointFileReader(ExperimentsForIS.rootDir + this.dataset + "/witness.dat", false).subList(0,
				this.noOfWitnessData);
	}

	public int getNoOfRefPoints() {
		return noOfRefPoints;
	}

	public Metric<CartesianPoint> getMetric() {
		return metric;
	}

}
