package extreme_fourpoint;

import java.util.List;

import eu.similarity.msc.core_concepts.CountedMetric;
import eu.similarity.msc.data.cartesian.CartesianPoint;
import eu.similarity.msc.data.testloads.TestContext;
import eu.similarity.msc.data.testloads.TestContext.Context;
import eu.similarity.msc.search.VPTree;

public class QueriesAndThresholds {

	public static void main(String[] a) throws Exception {
		int noOfQueries = 1000;
		TestContext tc = new TestContext(Context.colors);
		tc.setSizes(noOfQueries, 0);
		final List<CartesianPoint> data = tc.getData();
		final CountedMetric<CartesianPoint> metric = new CountedMetric<>(tc.metric());
		VPTree<CartesianPoint> vpt = new VPTree<>(data, metric);
		double[] qDists = new double[noOfQueries];
		int ptr = 0;
		final List<CartesianPoint> queries = tc.getQueries();
		for (CartesianPoint q : queries) {
			int res = vpt.nearestNeighbour(q);
			double d = metric.distance(q, data.get(res));
			qDists[ptr++] = d + 0.0000000000000001;
		}

		int checkRes = 0;
		metric.reset();
		for (int i = 0; i < noOfQueries; i++) {
			List<CartesianPoint> res = vpt.search(queries.get(i), qDists[i]);
			System.out.println(qDists[i] + "\t" + res.size());
			checkRes += res.size();
		}
		System.out.println(checkRes);
		System.out.println(metric.reset() / queries.size());
	}
}
