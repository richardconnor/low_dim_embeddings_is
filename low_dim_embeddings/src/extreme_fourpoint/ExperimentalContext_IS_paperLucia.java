package extreme_fourpoint;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.data.SiftMetricSpace;
import eu.similarity.msc.data.cartesian.CartesianPoint;
import eu.similarity.msc.data.testloads.CartesianPointGenerator;
import eu.similarity.msc.data.testloads.TestLoad;
import eu.similarity.msc.data.testloads.TestLoad.SisapFile;
import eu.similarity.msc.metrics.cartesian.Euclidean;
import eu.similarity.msc.search.VPTree;
import eu.similarity.msc.util.CartesianPointFileReader;

public class ExperimentalContext_IS_paperLucia {
	private static Random rand = new Random(0);
	private static Log log = LogFactory.getLog(ExperimentalContext_IS_paperLucia.class);

	public static double[] colorsThresholds = { 0.052, 0.083, 0.131 };
	public static double[] nasaThresholds = { 0.12, 0.285, 0.53 };
	private static double euc10threshold = 0.235107454;

	public static void createTestData(List<CartesianPoint> data, String dataName)
			throws Exception, FileNotFoundException {

		File f = new File(ExperimentsForISLucia.rootDir + dataName);
		if (!f.exists()) {
			f.mkdir();
		}

		printDataFile(data, dataName, "/pivots", 3000);
		printDataFile(data, dataName, "/witness", 5000);
		printDataFile(data, dataName, "/testdata", 1000);
		printDataFile(data, dataName, "/queries", 1000);
		printDataFile(data, dataName, "/bigdata", 10000);
	}

	/**
	 * create new NN file for the given dataset, format is: queryid: threshold for
	 * noOfNN. We are using Euclidean distance in all of these cases, bless us (i.e.
	 * this is just sloppy to get this done quickly!)
	 * 
	 * @param dataName
	 * @param noOfNN
	 * @throws Exception
	 */
	public static void createNNinfo(String dataName, int noOfNN) throws Exception {

		CartesianPointFileReader queries = new CartesianPointFileReader(
				ExperimentsForISLucia.rootDir + dataName + "/queries.dat", false);
		CartesianPointFileReader data = new CartesianPointFileReader(
				ExperimentsForISLucia.rootDir + dataName + "/bigdata.dat", false);

		Metric<CartesianPoint> euc = new Euclidean<>();
		VPTree<CartesianPoint> vpt = new VPTree<>(data, euc);

		PrintWriter pw = new PrintWriter(ExperimentsForISLucia.rootDir + dataName + "/nnInfo.dat");

		int ptr = 0;
		for (CartesianPoint q : queries) {
			List<Integer> res = vpt.nearestNeighbour(q, noOfNN);
			pw.print(ptr++ + "\t");
			for (int i : res) {
				pw.print("\t" + euc.distance(q, data.get(i)));
			}
			pw.println();

		}
		pw.close();
	}

	public static double[] getNNinfo(String dataName, int whichNN) throws Exception {
		LineNumberReader lnr = new LineNumberReader(
				new FileReader(ExperimentsForISLucia.rootDir + dataName + "/nnInfo.dat"));
		double[] res = new double[1000];
		int ptr = 0;
		for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
			Scanner s = new Scanner(line);
			s.useLocale(Locale.US);
			s.nextInt(); // strip id as they're currently all in order from 0
			for (int i = 0; i < whichNN; i++) {
				res[ptr] = s.nextDouble();
			}
			s.close();
			ptr++;
		}

		lnr.close();
		return res;
	}

	public static ExperimentalContext_IS_paperLucia easyColorsEc(int refPoints) {
		final ExperimentalContext_IS_paperLucia colors3NN = new ExperimentalContext_IS_paperLucia(DataSets.colors,
				refPoints);
		colors3NN.queryThreshold = colorsThresholds[0];
		colors3NN.kNN = true;
		colors3NN.k = 3;
		colors3NN.metric = new Euclidean<>();
		colors3NN.noOfResultsAtThreshold = 1858;
		colors3NN.filterThreshold = colorsThresholds[0];
		return colors3NN;
	}

	public static ExperimentalContext_IS_paperLucia easyNasaEc(int refPoints) {
		ExperimentalContext_IS_paperLucia ec = new ExperimentalContext_IS_paperLucia(DataSets.nasa, refPoints);
		ec.queryThreshold = 0.12;
		ec.kNN = true;
		ec.k = 3;
		ec.metric = new Euclidean<>();
		ec.noOfResultsAtThreshold = 1308;
		ec.filterThreshold = 0.12;
		return ec;
	}

	public static ExperimentalContext_IS_paperLucia easyYfcc() {
		final ExperimentalContext_IS_paperLucia colors3NN = new ExperimentalContext_IS_paperLucia(DataSets.yfcc, 128);
		colors3NN.queryThreshold = 0.75;
		colors3NN.kNN = true;
		colors3NN.k = 3;
		colors3NN.metric = new Euclidean<>();
		colors3NN.noOfResultsAtThreshold = 117;
		colors3NN.filterThreshold = 0.75;
		return colors3NN;
	}

	public static void setUpData(DataSets expData) throws Exception, FileNotFoundException {
		switch (expData) {
		case colors: {
			List<CartesianPoint> data = (new TestLoad(SisapFile.colors)).getData();
			createTestData(data, "colors");

			break;
		}
		case nasa: {
			List<CartesianPoint> data = (new TestLoad(SisapFile.nasa)).getData();
			createTestData(data, "nasa");

			break;
		}
		case euc10: {
			CartesianPointGenerator cg = new CartesianPointGenerator(10, false);
			List<CartesianPoint> data = new ArrayList<>();
			for (int i = 0; i < 20000; i++) {
				data.add(cg.next());
			}
			createTestData(data, "euc10");

			break;
		}
		case euc20: {
			CartesianPointGenerator cg = new CartesianPointGenerator(20, false);
			List<CartesianPoint> data = new ArrayList<>();
			for (int i = 0; i < 20000; i++) {
				data.add(cg.next());
			}
			createTestData(data, "euc20");

			break;
		}
		case sift: {
			String path = "/Volumes/Data/SIFT_mu/";
			SiftMetricSpace gds = new SiftMetricSpace(path);
			Map<Integer, float[]> sifts = gds.getData(20);

			List<CartesianPoint> data = new ArrayList<>();
			for (int i : sifts.keySet()) {
				float[] arr = sifts.get(i);
				data.add(new CartesianPoint(arr));
			}

			createTestData(data, "sift");

			break;
		}
		case alexnet: {
			Map<Integer, float[]> map = MFExperiments.getRawFile(0);
			List<CartesianPoint> data = new ArrayList<>();
			for (float[] fs : map.values()) {
				CartesianPoint cp = new CartesianPoint(fs);
				data.add(cp);
			}

			break;
		}
		case yfcc: {
			FileInputStream fis = new FileInputStream(ExperimentsForISLucia.YFCC_DATA_FILE);
			ObjectInputStream ois = new ObjectInputStream(fis);
			float[][] dat = (float[][]) ois.readObject();
			ois.close();
			List<CartesianPoint> data = new ArrayList<>();
			for (float[] floats : dat) {
				CartesianPoint cp = new CartesianPoint(floats);
				data.add(cp);
			}
			createTestData(data, "yfcc");

			break;
		}
		default: {
			log.fatal("setUpData: no initialisation defined for " + expData);
		}
		}
	}

	private static void printDataFile(List<CartesianPoint> data, final String dataSet, final String fileType,
			final int number) throws FileNotFoundException {

		PrintWriter pw = new PrintWriter(ExperimentsForISLucia.rootDir + dataSet + fileType + ".dat");

		for (int i = 0; i < number; i++) {
			CartesianPoint p = data.remove(rand.nextInt(data.size()));
			for (double d : p.getPoint()) {
				pw.print(d + "\t");
			}
			pw.println();
		}
		pw.close();
	}

	private int noOfRefPoints;
	private int noOfWitnessData;
	DataSets dataset;
	double queryThreshold;
	double createThreshold;
	double filterThreshold;

	boolean kNN;
	int k;
	int t;
	int noOfResultsAtThreshold;

	Metric<CartesianPoint> metric;

	ExperimentalContext_IS_paperLucia(DataSets data, int noOfRefPoints) {
		this.noOfRefPoints = noOfRefPoints;
		this.dataset = data;
		this.noOfWitnessData = 1000;
		this.queryThreshold = nasaThresholds[0];
		this.kNN = true;
		this.k = 3;
		this.metric = new Euclidean<>();
		this.noOfResultsAtThreshold = 0;
		this.filterThreshold = nasaThresholds[0];
	}

	public int calculateResultSetSize() throws Exception {
		List<CartesianPoint> queries = getQueries();
		List<CartesianPoint> dat = getBigData();

		VPTree<CartesianPoint> vpt = new VPTree<>(dat, this.metric);
		int res = 0;
		for (CartesianPoint q : queries) {
			res += vpt.search(q, this.queryThreshold).size();
		}
		return res;
	}

	public void generatePivotTableData(String pivSelectionMethod) throws Exception {

		List<CartesianPoint> pivs = this.getPivots();

		String pivotDistanceFile = ExperimentsForISLucia.rootDir + this.dataset + "/surrogates/pivotDists"
				+ this.noOfRefPoints + "_" + metric.getMetricName() + ".dat";

		RefPointsSelector<CartesianPoint> pc = null;
		switch (pivSelectionMethod) {
		case "largestDistanceToTheThirdNearest2DPoint":
			pc = new FourPointPivotsChooser<>(pivs, metric, 0, false, 0);
			break;
		case "random":
			pc = new RandomRefPointsChooser<>(pivs, metric);
			break;
		case "EdgarIdeaFirstClosest":
			pc = new EdgarIdeaFirstClosestPivotsChooser<>(pivs, metric);
			break;
		case "EdgarIdeaFirstFurthest":
			pc = new EdgarIdeaFirstFurthestPivotsChooser<>(pivs, metric);

			break;
		case "EdgarIdeaFirstClosestSecondFurthest":
			pc = new EdgarIdeaFirstClosestSecondFurthestPivotsChooser<>(pivs, metric);
			break;
		default:

			throw new Exception("Not implemented yet!");
		}
		pc.writePivotDistanceTable(pivotDistanceFile);

	}

	public List<CartesianPoint> getBigData() throws Exception {
		return new CartesianPointFileReader(ExperimentsForISLucia.rootDir + this.dataset + "/bigdata.dat", false);
	}

	public float[][] getPivotDistances(String pivSelectionMethod) throws ClassNotFoundException, IOException {

		String pivotDistTable = ExperimentsForISLucia.rootDir + this.dataset + "/surrogates/pivotDists"
				+ this.noOfRefPoints + "_" + metric.getMetricName() + ".dat";
		FileInputStream fis1 = new FileInputStream(pivotDistTable);
		ObjectInputStream ois1 = new ObjectInputStream(fis1);
		final float[][] dists = (float[][]) ois1.readObject();
		ois1.close();
		return dists;
	}

	public List<CartesianPoint> getPivots() throws Exception {
		return new CartesianPointFileReader(ExperimentsForISLucia.rootDir + this.dataset + "/pivots.dat", false)
				.subList(0, this.noOfRefPoints);
	}

	public double[] getNnInfo(int whichNN) throws Exception {
		LineNumberReader lnr = new LineNumberReader(
				new FileReader(ExperimentsForISLucia.rootDir + this.dataset + "/nnInfo.dat"));
		double[] res = new double[1000];
		int ptr = 0;
		for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
			Scanner s = new Scanner(line);
			s.useLocale(Locale.US);
			s.nextInt(); // strip id as they're currently all in order from 0
			for (int i = 0; i < whichNN; i++) {
				res[ptr] = s.nextDouble();
			}
			s.close();
			ptr++;
		}

		lnr.close();
		return res;
	}

	public List<CartesianPoint> getQueries() throws Exception {
		return new CartesianPointFileReader(ExperimentsForISLucia.rootDir + this.dataset + "/queries.dat", false);
	}

	public String getSurDataFileName() {
		final String dirName = ExperimentsForISLucia.rootDir + dataset + "/surrogates";
		File f = new File(dirName);
		if (!f.exists()) {
			f.mkdir();
		}
		final String fname = dirName + "/sur_" + this.noOfRefPoints + "_" + this.noOfWitnessData + "_"
				+ this.metric.getMetricName() + (this.kNN ? "_nn" + this.k : "_t" + this.t) + ".dat";
		return fname;
	}

	public String getSurDataFileName(String pivSelectionMethod) {
		final String dirName = ExperimentsForISLucia.rootDir + dataset + "/surrogates";
		File f = new File(dirName);
		if (!f.exists()) {
			f.mkdir();
		}
		final String fname = dirName + "/sur_" + this.noOfRefPoints + "nRO_" + this.noOfWitnessData + "witness_"
				+ this.metric.getMetricName() + "_" + pivSelectionMethod + (this.kNN ? "_nn" + this.k : "_t" + this.t)
				+ ".dat";
		return fname;
	}

	public List<CartesianPoint> getTestData() throws Exception {
		return new CartesianPointFileReader(ExperimentsForISLucia.rootDir + this.dataset + "/testdata.dat", false);
	}

	public List<CartesianPoint> getWitnesses() throws Exception {
		return new CartesianPointFileReader(ExperimentsForISLucia.rootDir + this.dataset + "/witness.dat", false)
				.subList(0, this.noOfWitnessData);
	}

	public void setThresholds(double d) {
		this.queryThreshold = d;
		this.filterThreshold = d;
	}

}
