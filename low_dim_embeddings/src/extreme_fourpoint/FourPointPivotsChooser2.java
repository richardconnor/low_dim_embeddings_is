package extreme_fourpoint;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.simplices.NSimplex;
import eu.similarity.msc.util.OrderedList;

public class FourPointPivotsChooser2<T> extends RefPointsSelector<T> {

	private class Pivot {

		public int pivotId;
		List<double[]> sampleData;
		private NSimplex<T> simp;
		public int pivId1;
		public int pivId2;

		public Pivot(int id1, int id2, int pivotId) {
			this.pivId1 = id1;
			this.pivId2 = id2;
			this.pivotId = pivotId;
			List<T> pivs = new ArrayList<>();
			pivs.add(FourPointPivotsChooser2.this.refPoints.get(id1));
			pivs.add(FourPointPivotsChooser2.this.refPoints.get(id2));
			this.simp = new NSimplex<>(FourPointPivotsChooser2.this.metric, pivs);
		}

		public double nnRadius(T point, double[] pointToPivDists, int nn, Metric<double[]> l2) {
			// find coordinates in plot
			double d1 = pointToPivDists[this.pivId1];
			double d2 = pointToPivDists[this.pivId2];
			double[] ds = { d1, d2 };
			double[] ap = this.simp.getApex(ds);

			double maxRad = getMaxRadius(nn, ap, l2);

			return maxRad;
		}

		/**
		 * @param nn nearest neighbour for distance
		 * @param ap the apex of the data point
		 * @return the radius of the nth nearest point to ap
		 */
		@SuppressWarnings("boxing")
		private double getMaxRadius(int nn, double[] ap, Metric<double[]> l2) {
			OrderedList<Object, Double> ol = new OrderedList<>(nn);
			for (double[] d : this.sampleData) {
				double dist = l2.distance(ap, d);
				ol.add(null, dist);
			}
			return ol.getComparators().get(nn - 1);
		}

		public double[] getApex(double[] dists) {
			return this.simp.getApex(dists);
		}

		public void setSample(List<T> sample, double[][] distTable) {
			this.sampleData = new ArrayList<>();
			for (int samp = 0; samp < sample.size(); samp++) {
				double d1 = distTable[samp][this.pivId1];
				double d2 = distTable[samp][this.pivId2];
				double[] ds = { d1, d2 };
				double[] newp = this.simp.getApex(ds);
				this.sampleData.add(newp);
			}
		}
	}

	private List<Pivot> pivots;
	private Set<Integer> bestPivots;
	private int K_FOR_KNN;

	public FourPointPivotsChooser2(List<T> refPoints, Metric<T> metric, int kForkNN) {
		super(refPoints, metric);
		this.pivots = new ArrayList<>();
		this.K_FOR_KNN = kForkNN;
	}

	@SuppressWarnings("boxing")
	@Override
	public void setWitnesses(List<T> witnesses) {

		double[][] sampleToPivDists = new double[witnesses.size()][this.refPoints.size()];

		for (int samp = 0; samp < witnesses.size(); samp++) {
			for (int piv = 0; piv < this.refPoints.size(); piv++) {
				sampleToPivDists[samp][piv] = this.metric.distance(witnesses.get(samp), this.refPoints.get(piv));
			}
		}

		int nextPivotId = 0;
		for (int i = 0; i < this.refPoints.size() - 1; i++) {
			for (int j = i + 1; j < this.refPoints.size(); j++) {
				if (this.bestPivots == null || this.bestPivots.contains(nextPivotId)) {
					final Pivot pivot = new Pivot(i, j, nextPivotId);
					pivot.setSample(witnesses, sampleToPivDists);
					this.pivots.add(pivot);
				}
				nextPivotId++;
			}
		}
		System.out.println("pivot table size: " + this.pivots.size());
	}

	@Override
	public DataPoint<T> getDataPoint(T datum) {
		DataPoint<T> result = new DataPoint<>();

		result.datum = datum;

		double[] refDists = getRefDists(datum);

		Pivot p = getBestPivot(datum, refDists, result);

		result.pivotId = p.pivotId;

		int[] sd = { p.pivId1, p.pivId2 };
		result.refs = sd;

		double[] dds = { refDists[p.pivId1], refDists[p.pivId2] };
		result.dists = dds;
		result.apex = p.getApex(dds);

		return result;
	}

	public double[] getRefDists(T datum) {
		double[] pointToPivDists = new double[this.refPoints.size()];
		for (int i = 0; i < this.refPoints.size(); i++) {
			pointToPivDists[i] = this.metric.distance(datum, this.refPoints.get(i));
		}
		return pointToPivDists;
	}

	/**
	 * @param datum
	 * @param refDists
	 * @param dp       horrible hack, passing this in just to get side-effect to
	 *                 track density outside the context
	 * @return
	 */
	@SuppressWarnings("boxing")
	private Pivot getBestPivot(T datum, double[] refDists, DataPoint<T> dp) {

		double bestRadius = 0;
		Pivot bestPivot = null;
		final Metric<double[]> l2 = ExtremeFourPoint.getL2();
		for (Pivot piv : this.pivots) {
			if (this.bestPivots == null || this.bestPivots.contains(piv.pivotId)) {
				double d = piv.nnRadius(datum, refDists, this.K_FOR_KNN, l2);
				if (d >= bestRadius) {
					bestRadius = d;
					bestPivot = piv;
					dp.radius = d;
				}
			}
		}

		return bestPivot;
	}

	public double[] pivotApex(int pivotId, double[] dists) {
		return this.pivots.get(pivotId).getApex(dists);
	}

	@Override
	public void writePivotDistanceTable(String filename) throws IOException {
		float[][] dists = new float[this.refPoints.size()][this.refPoints.size()];
		for (int i = 0; i < this.refPoints.size() - 1; i++) {
			for (int j = i + 1; j < this.refPoints.size(); j++) {
				double d = this.metric.distance(this.refPoints.get(i), this.refPoints.get(j));
				dists[i][j] = (float) d;
			}
		}
		OutputStream fw = new FileOutputStream(filename);
		ObjectOutputStream oos = new ObjectOutputStream(fw);
		oos.writeObject(dists);
		oos.close();
	}

	public void setBestPivots(Set<Integer> bestPivots) {
		this.bestPivots = bestPivots;
	}

}
