package extreme_fourpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.simplices.NSimplex;

public class RandomRefPointsChooser<T> extends RefPointsSelector<T> {

	

	private class Pivot {

		int pivotId;
		private NSimplex<T> simp;
		int pivId1;
		int pivId2;


		public Pivot(int id1, int id2, int pivotId) {
			this.pivId1 = id1;
			this.pivId2 = id2;
			this.pivotId = pivotId;
			List<T> pivs = new ArrayList<>();
			pivs.add(RandomRefPointsChooser.this.refPoints.get(id1));
			pivs.add(RandomRefPointsChooser.this.refPoints.get(id2));
			this.simp = new NSimplex<>(RandomRefPointsChooser.this.metric, pivs);
		}
		public double[] getApex(double[] dists) {
			return this.simp.getApex(dists);
		}
	}

	List<Pivot> pivots;
	

	public RandomRefPointsChooser(List<T> refPoints, Metric<T> metric) {
		super(refPoints, metric);
		this.pivots = new ArrayList<>();
	}

	@Override
	public void setWitnesses(List<T> witnesses) { 
		for (int i = 0; i < this.refPoints.size() - 1; i++) {
			for (int j = i + 1; j < this.refPoints.size(); j++) {
				final Pivot pivot = new Pivot(i, j, this.pivots.size());
				this.pivots.add(pivot);
			}
		}
	}

	@Override
	public DataPoint<T> getDataPoint(T datum) {
		DataPoint<T> result = new DataPoint<>();

		result.datum = datum;

		double[] refDists = getRefDists(datum);

		Random rnd = new Random();
		Pivot p =this.pivots.get(rnd.nextInt(this.pivots.size()));
		
		result.pivotId = p.pivotId;

		int[] sd = { p.pivId1, p.pivId2 };
		result.refs = sd;

		double[] dds = { refDists[p.pivId1], refDists[p.pivId2] };
		result.dists = dds;
		result.apex = p.getApex(dds);

		return result;
	}

	
	public double[] getRefDists(T datum) {
		double[] pointToPivDists = new double[this.refPoints.size()];
		for (int i = 0; i < this.refPoints.size(); i++) {
			pointToPivDists[i] = this.metric.distance(datum, this.refPoints.get(i));
		}
		return pointToPivDists;
	}



	public double[] pivotApex(int pivotId, double[] dists) {
		return this.pivots.get(pivotId).getApex(dists);
	}

//	public void writePivotDistanceTable(String filename) throws IOException {
//		float[][] dists = new float[this.refPoints.size()][this.refPoints
//				.size()];
//		for (int i = 0; i < this.refPoints.size() - 1; i++) {
//			for (int j = i + 1; j < this.refPoints.size(); j++) {
//				double d = this.metric.distance(this.refPoints.get(i),
//						this.refPoints.get(j));
//				dists[i][j] = (float) d;
//				dists[j][i] = (float) d;//added
//			}
//		}
//		OutputStream fw = new FileOutputStream(filename);
//		ObjectOutputStream oos = new ObjectOutputStream(fw);
//		oos.writeObject(dists);
//		oos.close();
//	}
}
